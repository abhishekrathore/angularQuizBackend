var app = angular.module('myApp')
    .controller('reviewAllQuestionsCtrl', reviewAllQuestionsCtrl)
function reviewAllQuestionsCtrl(allApp,Paper,question,DeleteQuestion) {
    var reviewAllQuestionsCtrl=this;
    var allQuestions=[];
    allApp.then(function(data){
        console.log(data);
            reviewAllQuestionsCtrl.appNameArray=data;
        console.log(reviewAllQuestionsCtrl.appNameArray);
    })
    reviewAllQuestionsCtrl.selectApp = function(appData){
        console.log(appData);
        console.log(appData._id);
        reviewAllQuestionsCtrl.name=appData.appName;


        Paper.query({appId:appData._id},function(data){
            console.log(data);
            reviewAllQuestionsCtrl.papers=data;

        })
            ,function(err){
            console.log(err);
        }
        question.get({appType:appData._id},function(data){
            console.log(data);
            reviewAllQuestionsCtrl.questions=data.question;
            allQuestions=reviewAllQuestionsCtrl.questions;  //use for filter by topic/paper

        })
            ,function(err){
            console.log(err);
        }
    }
    reviewAllQuestionsCtrl.selectPaper = function(paper){
        console.log(paper._id);
    }

    reviewAllQuestionsCtrl.deleteQuestion = function(question){
        BootstrapDialog.show({
            message: 'Are you sure to save all questions',
            buttons: [{
                label: 'yes',
                // no title as it is optional
                cssClass: 'btn-primary',
                action: function(dialogItself){
                    console.log(question._id);
                    DeleteQuestion.get({questionId:question._id}, function (success) {
                        console.log(success);
                    } ,function(err)
                    {
                        console.log(err);
                    })
                    var index= _.indexOf(reviewAllQuestionsCtrl.questions,question);
                    reviewAllQuestionsCtrl.questions.splice(index,1);
                    dialogItself.close();
                }
            }, {
                label: 'no',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });

    }

    var tempQuestions=reviewAllQuestionsCtrl.questions;
    console.log(allQuestions);
    var temparray=[];

    reviewAllQuestionsCtrl.filterByPaperOrTopic = function(paper){
        reviewAllQuestionsCtrl.questions=allQuestions;
        reviewAllQuestionsCtrl.paperForShow=paper;
        console.log(paper);
         console.log(reviewAllQuestionsCtrl.questions);
        if(paper.type=="paper"){
            reviewAllQuestionsCtrl.questions= _.filter(reviewAllQuestionsCtrl.questions,function(ques){
                return (ques.paperId==paper._id);
            })
        }
        if(paper.type=="topic")
        {
            _.each(reviewAllQuestionsCtrl.questions,function(ques){
                _.each(ques.category,function(q){
                    if(q==paper._id){
                        temparray.push(ques);
                    }
                })
            })
            reviewAllQuestionsCtrl.questions=temparray;
             temparray=[];
        }
    }

}