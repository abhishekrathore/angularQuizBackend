angular.module('myApp')
    .factory('Question', function ($resource) {
        return $resource( '/question')
    })
    .factory('AppName', function ($resource) {
        return $resource( '/appName')
    })
    .factory('Paper', function ($resource) {
        return $resource( '/paper?appId=:appId')
    })
    .factory('DeleteQuestion', function ($resource) {
        return $resource( '/deleteQuestion?questionId=:questionId')
    })
    .factory('question', function($resource) {
        return $resource('/question?appType=:appType')
    })
    .service('allApp', function (AppName,$q) {
          var allAppName;
         var defer = $q.defer();
          AppName.query({},function(data){
              console.log(data);
              allAppName=data;
              defer.resolve(allAppName);

          })
        ,function(err){
              console.log(err);
          }

        return defer.promise;
    })
    .factory('questionArray', function () {
    return {questions:[]};
    })




