'use strict';
// Declare app level module which depends on filters, and services

var app = angular.module('myApp', [
    'ngRoute', 'angular.filter', 'ngResource','LocalForageModule'
])
//app.constant('catApi','http://ec2-35-161-139-251.us-west-2.compute.amazonaws.com:8888');
// app.constant('catApi', 'http://localhost:8888');


app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            'responseError': function (rejection) {
                if (rejection.status == 302) {
                    if (JSON.parse(rejection.data).redirect == 'spOrders') {
                        $location.path('/orders');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'allOrders') {
                        $location.path('/allorders');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'spLogin') {
                        $location.path('/splogin');
                    }
                }
                return $q.reject(rejection);
            }
        };
    });

    $routeProvider
        .when('/app', {
            templateUrl: 'partials/appName'
        })
        .when('/addpaper', {
            templateUrl: 'partials/addPaper'
        })
        .when('/questions', {
            templateUrl: 'partials/questions'
        })
        .when('/adminpanel', {
            templateUrl: 'partials/adminpanel'
        })
        .when('/reviewAllQuestions', {
            templateUrl: 'partials/reviewAllQuestions'
        })
        .otherwise({
            redirectTo: '/questions'
        });
    $locationProvider.html5Mode(true);
})
    .run(function ($http, questionArray,$localForage) {
        $localForage.getItem('paper').then(function (data) {
            console.log(data);
            if(data!=null){
                console.log(data);
                _.each(data,function(d){
                    questionArray.questions.push(d);
                })
                ;
            }

        })

    })














