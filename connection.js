var mongoose = require('mongoose')
    , Schema = mongoose.Schema
mongoose.connect('mongodb://localhost:27017/quizDB');

var dbmongoose = mongoose.connection;
dbmongoose.on('error', console.error.bind(console, 'connection error:'));
dbmongoose.once('open', function (callback) {
});

module.exports = {
    mongoose: mongoose,
    Schema: Schema
};