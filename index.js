/**
 * Module dependencies
 */

var done = false;
var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    http = require('http'),
    routes = require('./routes'),
    api = require('./routes/api'),
    path = require('path'),
    fs = require('node-fs');

// Constants
var DEFAULT_PORT = 8888;
var PORT = process.env.PORT || DEFAULT_PORT;

var QuestionController = require('./controllers/QuestionController');
var AppNameController = require('./controllers/AppNameController');
var PaperController = require('./controllers/PaperController');



// App
var app = express();
app.set('views', __dirname + '/views');
//app.set('view engine', 'jade');
app.set('view engine', 'ejs');
app.set("jsonp callback", true);
app.set('jsonp callback name', 'code');
app.use(function (req, res, next) {
    //console.log('%s %s %s', req.method, req.url, req.path);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: 'this is a secret key',
    resave: false,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Routes
 */


app.options('*', function (req, res) {
    res.sendStatus(200);
});



// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name',  routes.partials);
//
app.post('/question',QuestionController.addQuestion);
app.get('/question',QuestionController.getQuestionByAppId);
app.get('/deleteQuestion',QuestionController.deleteQuestionId);
app.get('/deletedQuestionsByAppType',QuestionController.deletedQuestionsByAppType);

//app.get('/getQuestionByAppId',QuestionController.getQuestionByAppId);

app.post('/paper',PaperController.addPaper);
app.get('/paper',PaperController.getPaper);
app.post('/appName',AppNameController.addApp);
app.get('/appName',AppNameController.getApp);



// JSON API

app.get('/api/name', api.name);
app.get('*', routes.index);
app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
