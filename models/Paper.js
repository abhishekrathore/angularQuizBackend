var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var PaperSchema= Schema({

        name: {type: String, required: true, trim: true,unique:true},
        imageUrl: {type: String, required: false, trim: true},
        appId: {type: String, required: true, trim: true},
        type: {type: String, required: true, trim: true},
    },
    {
        timestamps: true,
        collection: "Paper"
    });


module.exports = mongoose.model('Paper', PaperSchema);


