var Question = require('../models/Question');
var _ = require('underscore');


module.exports = {
    addQuestion: addQuestion,
    getQuestion: getQuestion,
    getQuestionByAppId: getQuestionByAppId,
    deleteQuestionId: deleteQuestionId,
    deletedQuestionsByAppType:deletedQuestionsByAppType
};
function addQuestion(req, res) {
    var question = req.body;
    var questionsArray = _.pluck(question, 'question');
    console.dir(questionsArray)
    Question.insertMany(questionsArray, function (err, result) {
        console.log(err);
        console.log(result);
        res.json({result: result});

    })
}


function getQuestion(req, res) {
    console.log('getQuestion');
    Question.find({deleted:false}, function (err, result) {
        if (err) {
            res.send(err);
        }
        res.json(result);

    })
}


function getQuestionByAppId(req, res) {
    var appType = req.query.appType;
    var lastFetchTime = req.query.lastFetchTime;
    var query;
    if (typeof lastFetchTime != 'undefined') {
        query = {
            appType: appType,
            deleted:false,
            updatedAt: {$gte: lastFetchTime}
        }
    }
    else {
        query = {
            deleted:false,
            appType: appType
        }
    }

    Question.find(query).lean().exec(function (err, result) {
        if (err) {
            res.send(err);
        }
        res.json({question: result, lastFetchTime: new Date()});

    })
}

function deleteQuestionId(req, res) {
    var quesId = req.query.questionId;
    console.log(quesId);
    Question.update({_id: quesId},
        {$set: {deleted: true}}, {upsert: false}, function (err, result) {
            res.json(result);
        })

}


function deletedQuestionsByAppType(req, res) {
    var appType = req.query.appType;
    console.log(appType);
    Question.find({appType:appType,deleted:true}).lean().select('_id').exec(function (err, result) {
            res.json(result);
        })

}
