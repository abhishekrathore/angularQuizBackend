var Paper = require('../models/Paper');
var _=require('underscore');


module.exports = {
    addPaper:addPaper,
    getPaper:getPaper
};
function addPaper(req,res)
{
    
    var paperInfo=req.body;
    var paperEntry=new Paper(paperInfo);
    paperEntry.save(function(err,result)
    {
        if(err) {
        console.log(err);

        res.json({err:'duplicate paper name'});
    }
        res.json(result);
    })

}



function getPaper(req,res)
{
    var appId=req.query.appId;
    Paper.find({appId:appId},function(err,result)
    {
        if(err)
        {
            res.send(err);
        }
        res.json(result);

    })
}
