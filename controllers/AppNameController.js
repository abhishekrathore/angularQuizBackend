var AppName = require('../models/AppName');
var _=require('underscore');


module.exports = {
    addApp:addApp,
    getApp:getApp
};
function addApp(req,res)
{
    var appInfo=req.body;
    console.dir(appInfo);
    var appEntry=new AppName(appInfo);
    appEntry.save(function(err,result)
    {
        if(err) {
            console.log(err);

            res.json({err:'duplicate app name'});
        }
        res.json(result);
    })
   
}



function getApp(req,res)
{
    AppName.find({},function(err,result)
    {
        if(err)
        {
            res.send(err);
        }
        res.json(result);

    })
}
